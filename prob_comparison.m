% collision probability comparison;
clear all;
close all;

t1 = []; t2 = []; t3 = []; t4 = []; t5 =[];

runs = 1;   % 50
for i = 1 : runs

robot_mean = [5.9,5];    % (4.1, 5)  and (5.9, 5)
cov        = [0.6, 0.01, 0.01, 0.01, 0.011, 0.01, 0.01, 0.01, 0.011];%first term 0.6 


object = [0.5,4,5,0.7, 0.1 , 0.1 , 0.7] ; % radius 0.5, 0.05 , x,y , covariance array 0.02, 0.01 , 0.01 , 0.02 && 0.7, 0.1 , 0.1 , 0.7

robot_radius = 0.3;   % 0.05 and 0.3

[p_park, t_park]       = park_collision_cost(robot_mean, cov, object, robot_radius);
[p, t_p]               = collision_cost(robot_mean, cov, object, robot_radius);
[p_dutoit, t_dutoit]   = dutoit_collision_cost(robot_mean, cov, object, robot_radius);
[p_zhu, t_zhu]         = zhu_collision_cost(robot_mean, cov, object, robot_radius);
[p_lambert, t_lambert] = lambert_collision_cost(robot_mean, cov, object, robot_radius);
[p_mci, t_mci]         = numerical_integral(robot_mean, cov, object, robot_radius);


t1 = [t1; t_park];
t2 = [t2; t_p];
t3 = [t3; t_dutoit];
t4 = [t4; t_zhu];
t5 = [t5; t_lambert];

end

