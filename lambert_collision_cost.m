% Approach of Lambert et al.

function [probability, t] = lambert_collision_cost(r_mean, cov, obstacles, r_rad)
tic;
p = [obstacles(1,2) obstacles(1,3)];
o = [r_mean(1) r_mean(2)];

diff = o-p;

sigma_obs = [obstacles(1,4:5);obstacles(1,6:7)];

sigma_robot = [cov(1:2); cov(4:5)];

sigma_c = inv(inv(sigma_obs) + inv(sigma_robot));

mu_c = sigma_c * ( sigma_robot\p' + sigma_obs\o');

samples = 10000;
R1 = mvnrnd(o,sigma_robot,samples);
R2 = mvnrnd(p,sigma_obs,samples);

radius = obstacles(1,1);

p_condition = 0;
for i = 1 : samples
   
  %robot_point =  (R1(i,1) - o(1))^2 + (R1(i,2) - o(2))^2; 
  %obs_point   =  (R2(i,1) - p(1))^2 + (R2(i,2) - p(2))^2; 
  
  diff_samples = sqrt( (R1(i,1)-R2(i,1))^2 + (R1(i,2)-R2(i,2))^2);
  if ( diff_samples < radius+r_rad)
      p_condition = p_condition + 1;
  end
    
end

probability = p_condition/samples;
t = toc;
end
