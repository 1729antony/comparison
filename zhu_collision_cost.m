
% Approach of Zhu et al.
function [probability, t] = zhu_collision_cost(r_mean, cov, obstacles, r_rad)
    tic;
    p = [obstacles(1,2) obstacles(1,3)];
    o = [r_mean(1) r_mean(2)];
    
    diff = o-p;
    diff_norm = norm(diff);
    
    sigma_obs = [obstacles(1,4:5);obstacles(1,6:7)];
        
    sigma_robot = [cov(1:2); cov(4:5)];
    
    sigma_c = sigma_obs + sigma_robot;
    
    
    radius = obstacles(1,1);
    radius = radius + r_rad;
    
   
    numer = radius - (diff/diff_norm)*diff';
    denom = sqrt( 2*(diff/diff_norm)*sigma_c*(diff'/diff_norm) );
    
    probability = 0.5 + 0.5 * erf(numer/denom);
    t = toc;
end
