% monte carlo integration
function [probability, t] = numerical_integral(r_mean, cov, obstacles, r_rad)
tic;
p = [obstacles(1,2) obstacles(1,3)];
o = [r_mean(1) r_mean(2)];

diff = o-p;

sigma_obs = [obstacles(1,4:5);obstacles(1,6:7)];

sigma_robot = [cov(1:2); cov(4:5)];

samples = 10000;
R1 = mvnrnd(o,sigma_robot,samples);
R2 = mvnrnd(p,sigma_obs,samples);

radius = obstacles(1,1);

p_condition = 0;
for i = 1 : samples
    for j = 1 : samples
        
        diff_samples = sqrt( (R1(i,1)-R2(j,1))^2 + (R1(i,2)-R2(j,2))^2);
        
        if ( diff_samples < radius+r_rad)
            p_condition = p_condition + 1;
        end
        
    end
end
probability = p_condition/samples^2;
t = toc;
end