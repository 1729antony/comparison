
% Approach of Dutoit et al.
function [probability, t] = dutoit_collision_cost(r_mean, cov, obstacles, r_rad)
    tic;
    probability = [];
   
    
    p = [obstacles(1,2) obstacles(1,3)];
    o = [r_mean(1) r_mean(2)];
    
    diff = o-p;
    sigma_obs = [obstacles(1,4:5);obstacles(1,6:7)];
        
    sigma_robot = [cov(1:2); cov(4:5)];
    
    sigma_c = sigma_obs + sigma_robot;
    
       
    radius = obstacles(1,1);
    %radius = (radius + r_rad)^2;
    
    V      = (4/3)*pi*(radius + r_rad)^3;
    
    
    
    probability = V*exp(-0.5*(diff/sigma_c)*diff')/sqrt(det(2*pi*sigma_c));
    t = toc;
end
