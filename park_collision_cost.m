
% Approach of Park et al.
function [probability, t] = park_collision_cost(r_mean, cov, obstacles, r_rad)
    tic;
    probability = [];
    for i = 1:length(obstacles(:,1))
    
    p = [obstacles(i,2) obstacles(i,3)];
    o = [r_mean(1) r_mean(2)];
    
    sigma1 = [obstacles(i,4:5);obstacles(i,6:7)];
    sigma_robot = [cov(1:2); cov(4:5)];
    sigma = sigma1 +sigma_robot;
    S = inv(sigma);
    
    
    
    radius = obstacles(i,1);
    radius = (radius + r_rad)^2;
    
    V      = (4/3)*pi*(radius + r_rad)^3;
    
    syms lambda
    
    A = S + lambda*eye(2);
    B = sigma\p' + lambda*o';
    
    eqn = (A\B - o')'*(A\B -o') - radius;
    
    sol_set = solve(eqn,lambda);
    sol_set = eval(sol_set);
   for j = 1 : length(sol_set)
        
        A = S + sol_set(j)*eye(2);
        B = sigma\p' + sol_set(j)*o';
        
        diff = (A\B - p');
        
        F = V*exp(-0.5*diff'/sigma*diff)/sqrt(det(2*pi*sigma));
        probability = [probability; F];
    end
    
    end
    
    probability = max(probability);
    t = toc;
%The above approach can sometimes give the lambda for minimum prob value, to get maximum the following is performed.     
    r1 = sqrt(radius);
    
    prob = [];
    for i = 0:0.01:2*pi
       x1 = o(1) + r1*cos(i);
       y1 = o(2) + r1*sin(i);
       
       diff1 = [x1;y1] - p';
       
       F1 = V*exp(-0.5*diff1'/sigma*diff1)/sqrt(det(2*pi*sigma));
       
       prob = [prob;F1];
    
    end
    prob1 = max(prob);
    probability = max(probability, prob1);
end
