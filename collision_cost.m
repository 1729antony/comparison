function [F_cdf, t] = collision_cost(mean, cov, obstacles, r_rad)
tic;
F  = [];
F_cdf = 0;

for i = 1:length(obstacles(:,1))
    
    mu = [mean(1) mean(2)] - [obstacles(i,2) obstacles(i,3)];
    sigma = [cov(1:2); cov(4:5)] + [obstacles(i,4:5);obstacles(i,6:7)];
    
    radius = obstacles(i,1);
    radius = (radius + r_rad)^2;
    
    % since symmetric matrix, we can find V such that V*V' = I and
    % V*Sigma*V' is diagonal
    
    
    [V,D] = eig(sigma);
    
    lambda = [D(1) D(4)];
    
    B = chol(sigma,'lower') ;
    %     sigma_root = sqrtm(sigma);
    
    
    b = V'/B*mu';
    
  
    error = 10000;
    F_init = 0;
    j = 0;
    tmp = 0;

    while (true)
        
        F_cdf = F_cdf +  c_param(b,j,lambda)* (radius^(j+1))/gamma(j+2);
        j = j +1;

        F_init = F_cdf;
        
        % to avoid the anomaly, which might happen when sigma very very negligible 
        if (F_cdf < 1 && F_cdf >= 0)
            tmp = F_cdf;
            
        end
        if (F_cdf < 1e-50)
            break;
        end
        if (j > 15)
            F_cdf = tmp;
            break;
           
        end
        
     
    end
    
    F = [F;F_cdf];
    F_cdf = 0;
       
end

F_cdf = max(abs(F));
t = toc;
end


function value = d_param(b, k, lambda)
value = (-1)^k* 0.5 * ((1-k*b(1)^2)*(2*lambda(1))^(-k) +  (1-k*b(2)^2)*(2*lambda(2))^(-k));
end

function value = c_param(b,k,lambda)
value = 0;
if (k == 0)
    value = exp(-0.5*(b(1)^2 + b(2)^2))*(2*lambda(1))^(-0.5)*(2*lambda(2))^(-0.5);
else
    for i = 0:k-1
        value = value + d_param(b, k-i, lambda)*c_param(b,i, lambda);
    end
    value = value/k;
end
end
